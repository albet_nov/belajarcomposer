<?php

namespace Asep\BelajarMklibPhpHello;

interface Mobil
{
    function gas();
    function stop();
    function isi_bensin();
    function nyalakan_mesin();
    function matikan_mesin();
    function buka_pintu();
    function tutup_pintu();
}

trait MobilPublic
{
    public function gas()
    {
        echo "brrm...\n";
    }

    public function stop()
    {
        echo "...\n";
    }

    public function isi_bensin()
    {
        echo "Glug...glug...\n";
    }

    public function nyalakan_mesin()
    {
        echo "ON\n";
    }

    public function matikan_mesin()
    {
        echo "OFF\n";
    }

    public function buka_pintu()
    {
        echo "Terbuka\n";
    }

    public function tutup_pintu()
    {
        echo "Tertutup\n";
    }
}

class ModelMobil implements Mobil
{
    use MobilPublic;
}
