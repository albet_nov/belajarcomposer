<?php

namespace Asep\BelajarMklibPhpHello;

interface Motor
{
    function gas();
    function stop();
    function isi_bensin();
    function nyalakan_mesin();
    function matikan_mesin();
}

trait MotorPublic
{
    public function gas()
    {
        echo "brrm...\n";
    }

    public function stop()
    {
        echo "...\n";
    }

    public function isi_bensin()
    {
        echo "Glug...glug...\n";
    }

    public function nyalakan_mesin()
    {
        echo "ON\n";
    }

    public function matikan_mesin()
    {
        echo "OFF\n";
    }
}

class ModelMotor implements Motor
{
    use MotorPublic;
}
